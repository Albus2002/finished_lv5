#pragma once
#include<iostream>
#include<cstring>
#include<memory>
#include<cassert>
#include<vector>
#include "util.h"

using namespace std;

static SymbolTable symbol_table_init;//init table
static SymbolTable* symbol_table;//current

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST {
 public:
  // 用智能指针管理对象
  std::unique_ptr<BaseAST> func_def;
  KIR Dump() const override {
    string tmp = func_def->Dump().s;
    return KIR(tmp);
  }
};

// FuncDef 也是 BaseAST

class FuncDefAST : public BaseAST {
 public:
  unique_ptr<BaseAST> func_type;
  string ident;
  unique_ptr<BaseAST> block;
  vector<unique_ptr<BaseAST> > param_list;
  KIR Dump() const override {
    string s = func_type->Dump().s;
    if(debug){
        cout<<"FuncdefAST"<<endl;
        cout<<"func type : "<<func_type->Dump().s<<endl;
    }
    string type_id = ": " + s;
    symbol_table_init.Insert(ident,SymbolTableEntry(3));
    tmp_var_cnt = 0;

    SymbolTable* new_symbol_table = new SymbolTable(symbol_table);
    symbol_table = new_symbol_table;
    KIR block_dump = block->Dump();
    string tmp_ir = block_dump.s;
    if(block_dump.end == 0){
        tmp_ir += "  ret 0\n";
    }
    symbol_table = symbol_table->parent;
    return KIR("fun @" + ident + "()" + type_id + "{\n%entry:\n" + tmp_ir + "}\n");
  }
    
};

class FuncTypeAST : public BaseAST{
public:
 std::string func_type;
 KIR Dump() const override{
    string s = "";
    if(func_type=="int"){
        s += "i32 ";
    }
    return KIR(s);
 }

};

class BlockAST:public BaseAST{
public:
    vector<unique_ptr<BaseAST> > block_items;
    BlockAST(){
        block_items.clear();
    }
    KIR Dump() const override{
        if(debug){
            cout<<"BlockAST built, length : "<< block_items.size() <<endl;
        }
        SymbolTable *new_symbol_table = new SymbolTable(symbol_table);
        symbol_table = new_symbol_table;

        string tmp = "";
        KIR ret;
        for(auto &i : block_items){
            KIR block_ir = i->Dump();
            tmp += block_ir.s;
            if(block_ir.end == 1){
                ret.end = 1;
                break;
            }
        }
        ret.s = tmp;
        symbol_table = symbol_table->parent;
        free(new_symbol_table);
        return ret;
    }
};

class BlockItemAST : public BaseAST {
    public:
        int kind;//0-decl,1-stmt
        unique_ptr<BaseAST> decl;
        unique_ptr<BaseAST> stmt;
        KIR Dump() const override {
            if(kind == 0){
                if(debug){
                    cout << "decl dumping" << endl;
                }
                return decl->Dump();
            }
            else if(kind == 1){
                if(debug){
                    cout << "stmt dumping" << endl;
                }
                return stmt->Dump();
            }
            else{
                if(debug){
                    cout << "blockitemAST error" << endl;
                }
                assert(0);
            }
        }
};

class StmtAST : public BaseAST{
public:
    //std::unique_ptr<BaseAST> number;
    std::unique_ptr<BaseAST> matched_stmt;
    int kind;

    StmtAST() = default;
    KIR Dump() const override{
        if(debug){
            cout<<"building stmt"<<endl;
        }
        if(kind == 0){
            if(debug){
            cout<<"building Matchedstmt"<<endl;
            }
            return matched_stmt->Dump();
        }
        else{
            if(debug){
                cout<<"building stmt error,unknown type"<<endl;
            }
            return KIR("unknown stmt");
        }
    }
};

class MatchedStmtAST:public BaseAST{
    public:
    unique_ptr<BaseAST> exp;
    unique_ptr<BaseAST> lval;
    unique_ptr<BaseAST> block;
    int kind;//kind = 0 is exp
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"Matchedstmt, kind 0, exp"<<endl;
            }
            auto exp_dump = exp->Dump();
            string tmp_s = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            return KIR(tmp_s + " ret " + tmp_var + "\n",1);//end of control stream
        }
        else if(kind == 1){
            if(debug){
                cout<<"MatchedStmtAST,kind 1, lval"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;

            KIR lval_dump = lval->get_var_addr();
            tmp_ir.append(lval_dump.s);
            return KIR(tmp_ir + "  store " + tmp_var + ", " + lval_dump.tmp_var + "\n");
        }
        else if(kind == 2){
            if(debug){
                cout<<"MatchedStmtAST, kind 2,block "<<endl;
            }
            return block->Dump();
        }
        else if(kind == 3){
            if(debug){
                cout<<"MatchedStmtAST, kind 3,Exp "<<endl;
            }
            return exp->Dump();
        }
        else if(kind == 4){
            if(debug){
                cout<<"MatchedStmtAST, kind 4,empty "<<endl;
            }
            return KIR("");
        }
        else{
            if(debug){
                cout<<"MatchedStmtAST, kind unknown!"<<endl;
            }
            assert(0);
        }
    }
};

class LValAST : public BaseAST {
public:
    string id = "error";
    vector<unique_ptr<BaseAST> > array_index;
    LValAST(){};
    LValAST(string _i){
        this->id = _i;
    }
    KIR Dump() const override {
        string tmp_var = "%" + to_string(tmp_var_cnt++);
        //cout<<1111<<endl;
        SymbolTableEntry* entry = symbol_table->find(id);
        //cout<<111<<endl;
        if(entry != nullptr){
            //cout<<entry->kind<<endl;
            if(entry->kind == 1){
                //cout<<"1"<<endl;
                if(debug){
                    cout<<"LValAST, entry->kind 1\n";
                    cout<<"id : "<<id<<endl;
                    cout<< "entry addr: "<< entry->addr <<endl;
                }
                return KIR("  " + tmp_var + " = load " + entry->addr + "\n", tmp_var);
            }
            else if(entry->kind == 0){
                //cout<<"0"<<endl;
                if(debug){
                    cout<<"LValAST, entry->kind 0\n";
                }
                return KIR("",to_string(entry->constant));
            }
            else{
                //cout<<"else"<<endl;
                if(debug){
                    cout<<"LValAST, wrong var type\n";
                }
                assert(0);
            }
        }
        else{
            if(debug){
                cout<< "find nothing!"<<endl;
            }
            return KIR("  " + tmp_var + " = load @" + id + "\n",tmp_var);
        }
    }
    virtual KIR get_var_addr() override {
        SymbolTableEntry* entry = symbol_table->find(id);
        if(entry==nullptr){
            return KIR("","errNoAddr");
        }
        if(entry->kind == 1){
            return KIR("",entry->addr);
        }
        else{
            if(debug){
                cout<< " get_addr_error, kind :" << entry->kind<<endl;
            }
            assert(0);
        }
    }
    virtual int get_value() const override {
        SymbolTableEntry *entry = symbol_table->find(id);
        if(entry != nullptr && entry->kind == 0){
            return entry->constant;
        }
        else{
            return get_value_err;
        }
    }
};

class ConstExpAST : public BaseAST {
public:
    unique_ptr<BaseAST> exp;
    KIR Dump() const override{
        return exp->Dump();
    }
    virtual int get_value() const override{
        return exp->get_value();
    }
};

class VarExpAST : public BaseAST {
public:

};

class ConstDeclAST : public BaseAST {
    public:
        unique_ptr<BaseAST> btype;
        vector<unique_ptr<BaseAST> > const_defs;
        KIR Dump() const override {
            string tmp_cmd = "";
            string type = btype->Dump().s;
            for(auto &i: const_defs){
                KIR const_dump = i->Dump();
                tmp_cmd.append(const_dump.s);
            }
            return KIR(tmp_cmd,"");
        }
};

class BTypeAST : public BaseAST {
    public:
        int kind = 0;
        KIR Dump() const override {
            if(kind==0){
                return KIR("i32");
            }
            else{
                if(debug)
                {
                    cout<<"Btype error!"<<endl;
                }
                assert(0);
            }
        }
};

class ConstDefAST : public BaseAST {
    public:
    string id;
    vector<unique_ptr<BaseAST> > array_types;
    unique_ptr<BaseAST> exp;
    KIR Dump() const override {
        if(debug){
            cout<< " ConstDefAST, id : " << id <<endl;
        }
        if(array_types.size() == 0){
            int val = exp->get_value();
            SymbolTableEntry tmp_entry(0,val);
            //cout<<"kind:"<<tmp_entry.kind << "val:" <<tmp_entry.constant<<endl;
            symbol_table->Insert(id,tmp_entry);
            string s = id + " = " + to_string(val) + "\n";
            return KIR("",id);
        }
        else{
            if(debug){
                cout<< "array_type, not finished"<<endl;
            }
            assert(0);
        }
    }
};

class ConstInitValAST : public BaseAST {
    public:
    int kind;//0 - constexp;
    unique_ptr<BaseAST> const_exp;
    vector<unique_ptr<BaseAST> > const_init_vals;
    KIR Dump() const override{
        if(kind == 0)
        {
            return const_exp->Dump();
        }
        else{
            if(debug){
                cout<< "kind error of const init val ast"<<endl;
            }
            assert(0);
        }
    }
    virtual int get_value() const override {
        if(debug){
            cout<<"const init val ast get value"<<endl;
        }
        return const_exp->get_value();
    }
};

class VarDeclAST : public BaseAST {
    public:
    unique_ptr<BaseAST> btype;
    vector<unique_ptr<BaseAST> > var_defs;

    KIR Dump() const override {
        string tmp_cmd = "";
        string type = btype->Dump().s;
        for(auto &i : var_defs){
            tmp_cmd.append(i->Dump().s);
        }
        return KIR(tmp_cmd,"");
    }
};

class VarDefAST : public BaseAST {
    public:
    int kind;//0 - without init val,1 - with
    string id;
    vector<unique_ptr<BaseAST> > array_types;
    unique_ptr<BaseAST> exp;

    KIR Dump() const override {
        string block_num = "_" + to_string(symbol_table->num);
        string tmp_var = "@" + id + block_num;

        auto tmp_array_types = new vector<int>();
        for(auto &i: array_types){
            tmp_array_types->push_back(i->get_value());
        }
        if(kind == 0){
            if(debug){
                cout<<"VarDefAST dump ,kind :" <<kind<<endl;
            }
            string type = get_type_name(&array_types);
            if(block_cnt < 1){
                string tmp_cmd = "global " + tmp_var + " = alloc " + type + ", zeroinit\n";
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    if(debug){
                        cout << "array_type,not finished"<<endl;
                    }
                    assert(0);
                }
                return KIR(tmp_cmd,tmp_var);
            }
            else{
                string tmp_cmd = "  " + tmp_var + " = alloc " + type + "\n";
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    if(debug){
                        cout << "array_type,not finished"<<endl;
                    }
                    assert(0);
                }
                return KIR(tmp_cmd,tmp_var);
            }
        }
        else if(kind == 1){
            KIR exp_dump = exp->Dump(tmp_array_types);
            string type = get_type_name(&array_types);
            if(block_cnt < 1){
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("global " + tmp_var + " = alloc " + type + ", " + exp_dump.tmp_var + "\n");
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    if(debug){
                        cout << "array_type,not finished"<<endl;
                    }
                    assert(0);
                }
                return KIR(tmp_cmd,tmp_var);
            }
            else{
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("  " + tmp_var + " = alloc " + type + "\n");
                if(array_types.size()==0){
                    tmp_cmd.append("  store " + exp_dump.tmp_var + ", " + tmp_var + "\n");
                }
                else{
                    if(debug){
                        cout << "array_type,not finished"<<endl;
                    }
                    assert(0);
                }
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    if(debug){
                        cout << "array_type,not finished"<<endl;
                    }
                    assert(0);
                }
                return KIR(tmp_cmd,tmp_var);
            }

        }
        else{
            if(debug){
                cout << "VarDefAST dump error"<<endl;
            }
            assert(0);
        }
    }
};

class InitValAST : public BaseAST {
    public:
        int kind;//0-exp,1-{initvallist},2-{}
        unique_ptr<BaseAST> exp;
        vector<unique_ptr<BaseAST> > init_vals;
        virtual int get_value() const override {
            if(debug){
            cout<<"init val ast get value"<<endl;
            }
            return exp->get_value();
        }
        KIR Dump() const override{
            return exp->Dump();
        }
        KIR Dump(const vector<int> *array_types) const override {
            int k = array_types->size();
            if(k == 0){
                KIR exp_dump = exp->Dump();
                return exp_dump;
            }
            else{
                if(debug){
                    cout<<"array_type,not finished"<<endl;
                }
                assert(0);
            }
        }
};

class ExpAST : public BaseAST{
public:
    unique_ptr<BaseAST> exp;
    KIR Dump() const override{
        if(debug){
            cout<<"ExpAST"<<endl;
        }
        int value = exp->get_value();
        if(value != get_value_err){
            return KIR("",to_string(value));
        }
        else return exp->Dump();
    }
    virtual int get_value() const override {
        if(debug){
            cout<<"getting Value"<<endl;
        }
        int value = exp->get_value();
        if(debug){
            cout<<"get value of exp, value: "<<value<<endl;
        }
        return value;
    }
};

class LOrExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> lor_exp;// kind = 1,lor || land
    unique_ptr<BaseAST> land_exp;//kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();
        string false_label = new_label();
        string end_label = new_label();
        string tmp_var_1 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        string s = " " + tmp_var_1 + " = alloc i32\n  store 1, " + tmp_var_1 + "\n";
        
        KIR lor_dump = lor_exp->Dump();
        KIR land_dump = land_exp->Dump();
        
        s += lor_dump.s;

        string tmp_var_2 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  " + tmp_var_2 + " = eq " + lor_dump.tmp_var + ", 0\n";
        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label +"\n";
        
        s += true_label + ":\n";

        s += land_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += " " + tmp_var_3 + " = ne " + land_dump.tmp_var + ", 0\n";
        s += "  store " + tmp_var_3 + ", " +tmp_var_1 + "\n";
        
        string tmp_var_4 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump "+ end_label 
            + "\n" + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n";
        return KIR(s,tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"lorexpAST, kind 0"<<endl;
            }
            return land_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"lor||land,kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(false);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"lor exp ast get value,kind 0"<<endl;
            }
            return land_exp->get_value();
        }
        else if(kind==1){
            if(debug){
            cout<<"lor exp ast get value,kind 1"<<endl;
            }
            int left = lor_exp->get_value();
            int right = land_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left || right;
            }
        }
        else{
            if(debug){
                cout<<"lorexpast get value error"<<endl;
            }
            assert(0);
        }
    }
};

class LAndExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> land_exp; // kind = 1, land && eq
    unique_ptr<BaseAST> eq_exp; // kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();                                            
        string false_label = new_label();                                         
        string end_label = new_label();                                          
        string tmp_var_1 = "%" + to_string(tmp_var_cnt++);                    
        string s = "  " + tmp_var_1 + " = alloc i32\n  store 0, " + tmp_var_1 + "\n"; 

        KIR land_dump = land_exp->Dump();
        KIR eq_dump = eq_exp->Dump();

        s += land_dump.s; 

        string tmp_var_2 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_2 + " = ne " + land_dump.tmp_var + ", 0\n"; 

        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label
              + "\n";            
        s += true_label + ":\n";

        s += eq_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_3 + " = ne " + eq_dump.tmp_var + ", 0\n"; 
        s += "  store " + tmp_var_3 + ", " + tmp_var_1 + "\n";            

        string tmp_var_4 = "%" + to_string(tmp_var_cnt++); 
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump " + end_label + "\n"
              + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n"; //%4=%1
        
        return KIR(s, tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"landexpAST, kind 0"<<endl;
            }
            return eq_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"landexpAST, kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }
    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"land exp ast get value,kind 0"<<endl;
            }
            return eq_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"land exp ast get value,kind 1"<<endl;
            }
            int left = land_exp->get_value();
            int right = eq_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left && right;
            }
        }
        else{
            if(debug){
                cout<< "landExpAST get value error"<<endl;
            }
            assert(0);
        }
    }
};

class EqExpAST : public BaseAST{
public:
    int kind;
    //0 rel,1 == ,2 !=
    unique_ptr<BaseAST> eq_exp;
    unique_ptr<BaseAST> rel_exp;
    KIR generate_return() const{
        KIR exp_dump_1 = eq_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = rel_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if (debug) {
            cout << "CMD1: " << tmp_cmd_1<< endl;
            cout << "Var1: %" << tmp_var_1<< endl;
            cout << "CMD2: " << tmp_cmd_2 << endl;
            cout << "Var2: %" << tmp_var_2 << endl;
            cout << "VarNew: " << tmp_var_new << endl;
        }
        string op = " = eq ";
        if (kind == 1)
            op = " = eq ";
        else if (kind == 2)
            op = " = ne ";
        KIR ret = KIR(
            tmp_cmd_1 + tmp_cmd_2 + "  " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n", tmp_var_new);
        return ret;
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"eq_Exp_AST, kind 0"<<endl;
            }
            return rel_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"eq_Exp_AST, kind 1"<<endl;
            }
            return generate_return();
        }
        else if(kind==2){
            if(debug){
                cout<<"eq_Exp_AST, kind 2"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"eq exp ast get value,kind 0"<<endl;
            }
            return rel_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"eq exp ast get value,kind 1"<<endl;
            }
            int left = eq_exp->get_value();
            int right = rel_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left == right;
            }
        }
        else if(kind == 2){
            if(debug){
            cout<<"eq exp ast get value,kind 2"<<endl;
            }
            int left = eq_exp->get_value();
            int right = rel_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left != right;
            }
        }
        else{
            if(debug){
                cout<<"EqexpAST get value error"<<endl;
            }
            assert(0);
        }
    }
};

class RelExpAST: public BaseAST {
public:
    int kind;
    // 0 add, 1 < ,2 > ,3 <=, 4 >=
    unique_ptr<BaseAST> rel_exp;
    unique_ptr<BaseAST> add_exp;

    KIR generate_return() const{
        KIR exp_dump_1 = rel_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = add_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if(debug){
            cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
            cout << " tmp_var_1: " << tmp_var_1 << endl;
            cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
            cout << "tmp_var_2: " << tmp_var_2 << endl;
            cout << "tmp_var_new: " << tmp_var_new << endl;
        }
        string op = "";
        if(kind == 1){
            op = " = lt ";
        }
        else if(kind==2){
            op = " = gt ";
        }
        else if(kind==3){
            op = " = le ";
        }
        else if(kind == 4){
            op = " = ge ";
        }
        return KIR(tmp_cmd_1+tmp_cmd_2+" "+tmp_var_new+op+tmp_var_1+", "+tmp_var_2 +"\n",tmp_var_new);
    }
    KIR Dump() const override {
        if(kind==0){
            if(debug){
                cout << "relexpAST, kind 0" <<endl;
            }
            return add_exp->Dump();
        }
        else if(kind==1){
            if(debug){
                cout << "relexpAST, kind 1" <<endl;
            }
            return generate_return();
        }
        else if(kind == 2){
            if(debug){
                cout << "relexpAST, kind 2" <<endl;
            }
            return generate_return();
        }
        else if(kind==3){
            if(debug){
                cout << "relexpAST, kind 3" <<endl;
            }
            return generate_return();
        }
        else if(kind==4){
            if(debug){
                cout << "relexpAST, kind 4" <<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override{
        if(kind == 0){
            if(debug){
            cout<<"rel exp ast get value,kind 0"<<endl;
            }
            return add_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"rel exp ast get value,kind 1"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left < right;
            }
        }
        else if(kind == 2){
            if(debug){
            cout<<"rel exp ast get value,kind 2"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left > right;
            }
        }
        else if(kind == 3){
            if(debug){
            cout<<"rel exp ast get value,kind 3"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left <= right;
            }
        }
        else if(kind == 4){
            if(debug){
            cout<<"rel exp ast get value,kind 4"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left >= right;
            }
        }
        else{
            if(debug){
                cout<< "relexpast get value error" <<endl;
            }
            assert(0);
        }
    }
};

class PrimaryExpAST:public BaseAST{
    public:
    int kind;
    unique_ptr<BaseAST> exp;//kind = 0
    unique_ptr<BaseAST> number;//kind = 1
    unique_ptr<BaseAST> lval;//kind = 2

    KIR Dump() const override {
        if(debug){
            cout<<"PrimaryExpAST, kind "<< kind <<endl;
        }
        if(kind == 0){
            return exp->Dump();
        }
        else if(kind == 1){
            return number->Dump();
        }
        else if(kind==2){
            return lval->Dump();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 0 "<<endl;
            }
            return exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 1 "<<endl;
            }
            return number->get_value();
        }
        else if(kind==2){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 2 "<<endl;
            }
            return lval->get_value();
        }
        else{
            if(debug){
                cout<<"PrimaryAST get_value error"<<endl;
            }
            assert(0);
        }
    }
};

class AddExpAST : public BaseAST{
    public:
        unique_ptr<BaseAST> add_exp;
        unique_ptr<BaseAST> mul_exp;
        int kind;//0 mul,1 + ,2 -
        KIR generate_return() const {
            KIR exp_dump_1 = add_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = mul_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1 of addexpast: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1 of addexpast: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2 of addexpast: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2 of addexpast: " << tmp_var_2 << endl;
                cout << "tmp_var_new of addexpast: " << tmp_var_new << endl;
            }
            string op = " = add ";
            if(kind==1) op = " = add ";
            else if(kind == 2) op = " = sub ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump() const override{
            if(debug){
                cout<<"addexpAST, kind "<< kind <<endl;
            }
            if(kind == 0){
                return mul_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind == 2){
                return generate_return();
            }
            else{
                assert(false);
            }
        }
        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                return mul_exp->get_value();
            }
            else if( kind == 1){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                int left = add_exp->get_value();
                int right = mul_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left + right;
                }
            }
            else if(kind == 2){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                int left = add_exp->get_value();
                int right = mul_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left - right;
                }
            }
            else{
                if(debug){
                    cout<<"AddExpAST get value error"<<endl;
                }
                assert(0);
            }
        }
};

class MulExpAST : public BaseAST{
    public:
        int kind;
        //0 unary, 1 * ,2 / ,3 %
        unique_ptr<BaseAST> mul_exp;
        unique_ptr<BaseAST> unary_exp;
        KIR generate_return()const{
            KIR exp_dump_1 = mul_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = unary_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2: " << tmp_var_2 << endl;
                cout << "tmp_var_new: " << tmp_var_new << endl;
            }
            string op = " = mul ";
            if(kind==1) op = " = mul ";
            else if(kind == 2) op = " = div ";
            else if(kind==3) op = " = mod ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump()const override{
            if(debug){
                cout<<"mulexp,kind: "<<kind<<endl;
            }
            if(kind==0){
                return unary_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind==2){
                return generate_return();
            }
            else if(kind==3){
                return generate_return();
            }
            else{
                assert(0);
            }
        }
        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"MulAST get value,kind 0 "<<endl;
                }
                return unary_exp->get_value();
            }
            else if(kind == 1){
                if(debug){
                cout<<"MulAST get value,kind 1 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left * right;
                }
            }
            else if(kind == 2){
                if(debug){
                cout<<"MulAST get value,kind 2 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err || right == 0){
                    return get_value_err;
                }
                else{
                    return left / right;
                }
            }
            else if(kind == 3){
                if(debug){
                cout<<"MulAST get value,kind 3 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err || right == 0){
                    return get_value_err;
                }
                else{
                    return left % right;
                }
            }
            else{
                if(debug){
                    cout << " mulexp get_value error"<<endl;
                }
                assert(0);
            }
        }
};

class UnaryExpAST : public BaseAST{
    public:
        int kind;
        //0 prime
        //1 op exp
        unique_ptr<BaseAST> primary_exp;
        unique_ptr<BaseAST> unary_exp;
        unique_ptr<BaseAST> unary_op;
        KIR Dump()const override{
            if(kind==0){
                if(debug){
                    cout<<"UnaryAST,kind 0"<<endl;
                }
                return primary_exp->Dump();
            }
            else if(kind==1){
                if(debug){
                    cout<<"UnaryAST,kind 1"<<endl;
                }
                string op = unary_op->Dump().tmp_var;
                KIR exp_dump = unary_exp->Dump();
                string tmp_cmd = exp_dump.s;
                string tmp_var = exp_dump.tmp_var;
                string tmp_var_new = "%" + to_string(tmp_var_cnt++);
                if(debug){
                    cout<<"op: "<< op <<endl;
                    cout<<"tmp_cmd: "<<tmp_cmd<<endl;
                    cout<<"tmp_var: "<<tmp_var<<endl;
                    cout<<"tmp_var_new: "<<tmp_var_new<<endl;
                }
                if(op == "!"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = eq " + tmp_var +", 0 \n",tmp_var_new);
                }
                else if(op== "-"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = sub 0, " + tmp_var + "\n",tmp_var_new);
                }
                else if(op == "+"){
                    return exp_dump;
                }
                else{
                    cout<<"unknown op"<<endl;
                    assert(0);
                }
            }
            else{
                assert(0);
            }
        }

        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"unaryexpAST get value,kind 0 "<<endl;
                }
                return primary_exp->get_value();
            }
            else if(kind == 1){
                if(debug){
                cout<<"unaryexpAST get value,kind 1 "<<endl;
                }
                string op = unary_op->Dump().tmp_var;
                int ret = unary_exp->get_value();
                if(ret == get_value_err){
                    return get_value_err;
                }
                if(op == "!"){
                    return !ret;
                }
                else if(op == "-"){
                    return -ret;
                }
                else if(op == "+"){
                    return ret;
                }
                else{
                    if(debug){
                        cout<<"UnaryExpAST get value error, op error"<<endl;
                    }
                    assert(0);
                }
            }
            else{
                if(debug){
                    cout<<"UnaryexpAST get value error" <<endl;
                }
                assert(0);
            }
        }
};

class UnaryOpAST: public BaseAST{
    public:
    int kind;//0 +, 1 -,2 !
    string op;
    KIR Dump()const override{
        return KIR("",op);
    }
};

class NumberAST : public BaseAST {
public:
    // -- INT_CONST
    int int_const = 0;
    NumberAST() = default;
    NumberAST(int _int_const) : int_const(_int_const) {}

    KIR Dump() const override {
        string s = to_string(int_const);
        return KIR("",s);
    }

    virtual int get_value()const override{
        if(debug){
            cout<<"numberAST get value "<<endl;
        }
        return int_const;
    }
};

class DeclAST : public BaseAST{
    public:
        int kind;//0 const,1 var
        unique_ptr<BaseAST> const_decl;
        unique_ptr<BaseAST> var_decl;
        KIR Dump() const override{
            if(kind == 0){
                return const_decl->Dump();
            }
            else{
                return var_decl->Dump();
            }
        }
};


// ...

